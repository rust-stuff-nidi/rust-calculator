use std::io;

fn main() {
    println!("\nCALCULATOR\n");

    println!("Enter first number:");
    let mut first_number = String::new();
    io::stdin().read_line(&mut first_number)
    .expect("Failed to read number");

    println!("Enter operator:");
    let mut operator = String::new();
    io::stdin().read_line(&mut operator)
    .expect("Failed to read operator");

    println!("Enter second number:");
    let mut second_number = String::new();
    io::stdin().read_line(&mut second_number)
    .expect("Failed to read number");

    let first_number: i32 = first_number.trim().parse()
    .expect("Please enter a valid number!");
    let second_number: i32 = second_number.trim().parse()
    .expect("Please enter a valid number!");

    let solution;

    match operator.as_str().trim() {
        "+" => solution = first_number + second_number,
        "-" => solution = first_number - second_number,
        "*" => solution = first_number * second_number,
        "/" => solution = first_number / second_number,
        "%" => solution = first_number % second_number,
        _ => panic!("Invalid operator!")
    };

    println!("Solution: {}", solution);
}
